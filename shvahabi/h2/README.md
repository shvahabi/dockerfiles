# H2 RDBMS
## Introduction
Ubuntu 20.04 plus Oracle Java 14 and ready to host an H2 server instance of either future releases.
## Requirements
- [shvahabi/ubuntu-oracle-java-14:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-oracle-java-14/)
## Build
Before building change environment variable H2RELEASE_DATE value with proper release date to be inquired from H2 website, then:

`docker build -t shvahabi/h2:0.2 .`
