# Sbt, Scala Build Tool
## Introduction
Ubuntu 20.04 plus Oracle Java 14 and hosting sbt to play with scala projects. 
## Requirements
- [shvahabi/ubuntu-oracle-java-14:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-oracle-java-14/)
## Build
`docker build -t shvahabi/ubuntu-java-sbt:0.2 .`
