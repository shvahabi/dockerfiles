# Sbt and Python
## Introduction
Container baked with Ubuntu 20.04, Oracle Java 14, sbt and python3.
## Requirements
- [shvahabi/ubuntu-java-sbt:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-java-sbt/)
	- [shvahabi/ubuntu-oracle-java-14:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-oracle-java-14/)
## Build
`docker build -t shvahabi/ubuntu-java-sbt-python:0.2 .`
