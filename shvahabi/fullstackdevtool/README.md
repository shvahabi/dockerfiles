# A Full Stack Development Tools Collection
## Introduction
Ubuntu 20.04 plus Python3, Oracle Java 14, sbt and h2 RDBMS.
## Requirements
- [ubuntu:20.04](https://hub.docker.com/layers/ubuntu/library/ubuntu/20.04/images/sha256-93fd0705706e5bdda6cc450b384d8d5afb18fecc19e054fe3d7a2c8c2aeb2c83?context=explore)
## Build
- `docker build -t shvahabi/fullstackdevtool:0.1 --build-arg H2RELEASE_DATE="2019-03-13" .`
or whatever release date of future H2 you inquired from it's website.

Also you can change environment variable ORACLE\_JAVA\_VERSION to install upcoming Java
