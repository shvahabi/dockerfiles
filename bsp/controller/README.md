# BSP's SRD Controller
## Introduction
One in a collection of interdependent dockerfiles for deployment automation and beta testing of Simple Receive Dispatch, a project developed and maintained by Borna Sarv Padra.
## Caution
Please do build this image **strictly conforming** to BSP's SRD project documentation:
- [SRD_Controller](https://gitlab.com/shvahabi/simple-receive-dispatch-controller)

otherwise you will risk improper function and mess up with your data assets.
## Requirements
The following images shall be available on your local docker path:
- [shvahabi/ubuntu-java-sbt:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-java-sbt/)
	- [shvahabi/ubuntu-oracle-java-14:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-oracle-java-14/)
		- [ubuntu:20.04](https://hub.docker.com/layers/ubuntu/library/ubuntu/20.04/images/sha256-93fd0705706e5bdda6cc450b384d8d5afb18fecc19e054fe3d7a2c8c2aeb2c83?context=explore)

