# BSP's SRD
## Introduction
A collection of interdependent dockerfiles for deployment automation and beta testing of Simple Receive Dispatch, a project developed and maintained by Borna Sarv Padra.
## Caution
Please do build these images **strictly conforming** to BSP's SRD project documentation:
- [SRD_Model](https://gitlab.com/shvahabi/simple-receive-dispatch-model)
- [SRD_Controller](https://gitlab.com/shvahabi/simple-receive-dispatch-controller)
- [SRD_View](https://gitlab.com/shvahabi/simple-receive-dispatch-view)

otherwise you will risk improper function and mess up with your data assets.
## Requirements
The following images shall be available on your local docker path:
- [shvahabi/ubuntu-oracle-java-14:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-oracle-java-14/)
- [shvahabi/h2:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/h2/)
- [shvahabi/ubuntu-java-sbt:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-java-sbt/)
- [shvahabi/ubuntu-java-sbt-python:0.2](https://gitlab.com/shvahabi/dockerfiles/-/blob/master/shvahabi/ubuntu-java-sbt-python/)

refer to their documentation to build them.
